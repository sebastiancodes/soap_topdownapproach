package au.edu.unsw.soacourse.marketdata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.text.DecimalFormat;

import javax.jws.WebService;

@WebService(endpointInterface="au.edu.unsw.soacourse.marketservice.MarketDataUtilService")
public class MarketDataUtilServiceImpl implements MarketDataUtilService {

	ObjectFactory factory = new ObjectFactory();
	
	@Override
	public ConvertMarketDataResponse convertMarketData(
			ConvertMarketDataRequest parameters) throws ConvertMarketFaultMsg {
		ConvertMarketDataResponse res = factory.createConvertMarketDataResponse();
		
		String targetCurrency;
		//checking targetCurrency
		if (parameters.getTargetCurrency().matches("[a-zA-Z]{3}")) {
			//processing for URL parameters
			targetCurrency = parameters.getTargetCurrency();
		} else {
			String msg;
    		String code= "ERR_TARGET_CURRENCY";
			if (parameters.getTargetCurrency().length() != 3) {
	    		//assume that we only want a SEC value of length 3
	    		msg = "TARGET_CURRENCY code should be exactly 3 characters long";
			} else {
				msg = "TARGET_CURRENCY code should only have letters";
			}
			//SOAP Fault handling
    		//create a ServiceFaultType 'fault' from ObjectFactory
    		ServiceFaultType fault = factory.createServiceFaultType();
    		//prepare 'fault' object: set errorcode and errortext
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketFaultMsg(msg, fault);
		}
		
		// regex for checking dates
		String dayRegex = "(0[1-9])|([1-2][0-9])|(3[0-1])";
		String monthRegex = "(0[1-9]|1(0-2)";
		String yearRegex = "[0-9]{4}";
		String targetDate;

		//checking targetDate
		//TODO: might have to do checking for months w/ < 31 days
		if (parameters.getTargetDate().matches(yearRegex + "-" + monthRegex + "-" + dayRegex)) {
			//processing for URL parameters
			targetDate = parameters.getTargetDate();
    	} else {
    		String msg;
    		String code= "ERR_TARGET_DATE";
    		
    		if (!parameters.getTargetDate().matches("[0-9]{2}-[0-9]{2}-[0-9]{4}")) {
    			msg = "TARGET_DATE code should be in the form DD-MM-YYYY"; 		
    		} else {
    			msg = "TARGET_DATE code should have a valid date";
    		}
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketFaultMsg(msg, fault);
    	}
		
		String eventSetId = parameters.getEventSetId();
		// check if the file associated w/ this eventSetId exists
		File file = new File("putPathHere" + eventSetId + ".csv");
		if (!file.exists()) {
			// SOAP fault handling
			String code = "ERR_EVENTSETID";
			String msg = "There is no file associated with this eventSetId";
			ServiceFaultType fault = factory.createServiceFaultType();
			fault.setErrcode(code);
			fault.setErrtext(msg);
			throw new ConvertMarketFaultMsg(msg, fault);
		// the file exists so we'll extract data from it
		} else {
			try {
				
				//getting exchange rate for currency.
				URL currencyExcURL = new URL("http://www.xe.com/currencytables/?from=AUD&date=" + targetDate);
				BufferedReader in = new BufferedReader(new InputStreamReader(currencyExcURL.openStream()));
				double exchangeRate = 1; //just initialising it with a random value
				
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					//going to check if the currency we're looking for 
					//is there so we can extract the exchange rate
					//TODO: currently broken: doesn't allow automated extraction
					if (inputLine.contains(targetCurrency)) {
						// TODO: some more processing to get the exchange rate from the HTML
						String rate = "";
						exchangeRate = Double.parseDouble(rate);
					}
				}
				in.close();
				
				// the stuff for convertMarketData() below here works
				//going to read values from the file with the eventSetId and calculate new values
				Scanner sc = new Scanner(new FileReader(file));
				String finalOutput = sc.nextLine() + "\n"; //The first line isn't data
				while (sc.hasNextLine()) {
					String[] inputs = sc.nextLine().split(",");
					for (int i = 0; i < inputs.length; i ++) {
						if (i >= 2 && i <= 5 || i == 7) {
							double value = Double.parseDouble(inputs[i].substring(3));
							value = value * exchangeRate;
							DecimalFormat numberFormat = new DecimalFormat("#.00");
							inputs[i] = targetCurrency + numberFormat.format(value);
						}
					}
					for (int i = 0; i < inputs.length; i ++) {
						finalOutput += inputs[i];
						if (i != inputs.length - 1) {
							finalOutput += ",";
						}
					}
					finalOutput += "\n";
					
				}
				sc.close();
				
				//writing it back to the file 
				FileOutputStream fileOut = new FileOutputStream(file);
		        fileOut.write(finalOutput.getBytes());
		        fileOut.close();
		        
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return res;
	}

	@Override
	public SummariseMarketDataResponse summariseMarketData(
			SummariseMarketDataRequest parameters)
			throws SummariseMarketFaultMsg {
		
		SummariseMarketDataResponse res = factory.createSummariseMarketDataResponse();
		
		String eventSetId = parameters.getEventSetId();
		//check if the file associated w/ this eventSetId exists
		//TODO: find the path of where the data files will be stored and update
		File file = new File("putPathHere" + eventSetId + ".csv");
		if (!file.exists()) {
			//SOAP fault handling
			String code = "ERR_EVENTSETID";
			String msg = "There is no file associated with this eventSetId";
			ServiceFaultType fault = factory.createServiceFaultType();
			fault.setErrcode(code);
			fault.setErrtext(msg);
			throw new SummariseMarketFaultMsg(msg, fault);
		// the file exists so we'll extract data from it
		} else {
			try {
				Scanner sc = new Scanner(new FileReader(file));
				sc.nextLine(); //don't need to use first line
				String firstLine = sc.nextLine();
				String lastLine = "";
				String[] firstLineData = firstLine.split(",");
				res.setEventSetId(eventSetId);
				res.setSec(firstLineData[0]);
				res.setStartDate(firstLineData[1]);
				//refer to the convertcurrency part?
				//substr(a,b) goes from index a to b-1
				res.setCurrencyCode(firstLineData[2].substring(0, 3)); 
				while (sc.hasNext()) {
					lastLine = sc.nextLine();
				}
				String[] lastLineData = lastLine.split(",");
				res.setEndDate(lastLineData[1]);
				long fileSize = file.length(); //size of file in bytes
				//converting to appropriate sizes: in KB, MB, GB... etc
				String sizeEnding = "B";
				if (fileSize/(1024*1024*1024) >= 1) {
					fileSize = fileSize/(1024*1024*1024);
					sizeEnding = "GB";
				} else if (fileSize/(1024*1024) >= 1) {
					fileSize = fileSize/(1024*1024);
					sizeEnding = "MB";
				} else if (fileSize/(1024) >= 1) {
					fileSize = fileSize/(1024);
					sizeEnding = "KB";
				}
				res.setFileSize(Long.toString(fileSize) + sizeEnding);
				
				sc.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return res;
	}

	@Override
	public VisualiseMarketDataResponse visualiseMarketData(
			VisualiseMarketDataRequest parameters)
			throws VisualiseMarketFaultMsg {
		// TODO Auto-generated method stub
		return null;
	}

}
