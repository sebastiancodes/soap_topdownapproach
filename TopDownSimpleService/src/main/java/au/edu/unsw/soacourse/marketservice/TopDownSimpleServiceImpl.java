package au.edu.unsw.soacourse.marketservice;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface="au.edu.unsw.soacourse.marketservice.TopDownSimpleService")
public class TopDownSimpleServiceImpl implements TopDownSimpleService {

	ObjectFactory factory = new ObjectFactory();

	@Override
	public ImportMarketDataResponse importMarketData(
			ImportMarketDataRequest parameters) throws ImportMarketFaultMsg {
		//URL parameters for downloading the right data:
		// g and ignore are always the same
		String s, a, b, c, d, e, f;
		String eventSetID;

		//check for errors in input here and return faultmsg as appropriate	
		
		//checking sec
		if (parameters.getSec().matches("[a-zA-Z]{3}")) {
			//processing for URL parameters
			s = parameters.getSec();
		} else {
			String msg;
    		String code= "ERR_SEC";
			if (parameters.getSec().length() != 3) {
	    		//assume that we only want a SEC value of length 3
	    		msg = "SEC code should be exactly 3 characters long";
			} else {
				msg = "SEC code should only have letters";
			}
			//SOAP Fault handling
    		//create a ServiceFaultType 'fault' from ObjectFactory
    		ServiceFaultType fault = factory.createServiceFaultType();
    		//prepare 'fault' object: set errorcode and errortext
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		//throw new importMarketFaultMsg(msg,fault)
    		throw new ImportMarketFaultMsg(msg, fault);
		}
		
		// regex for checking dates
		String dayRegex = "(0[1-9])|([1-2][0-9])|(3[0-1])"; 
		String monthRegex = "(0[1-9]|1(0-2)"; 
		String yearRegex = "[0-9]{4}";
		
		//checking startDate
		//TODO: might have to do checking for months w/ < 31 days
		if (parameters.getStartDate().matches(dayRegex + "-" + monthRegex + "-" + yearRegex)) {
			//processing for URL parameters
			String[] date = parameters.getStartDate().split("-");
			b = date[0];
			//adjusting the month since January is 00
			a = Integer.toString(Integer.parseInt(date[1]) - 1);
			c = date[2];
    	} else {
    		String msg;
    		String code= "ERR_START_DATE";
    		
    		if (!parameters.getStartDate().matches("[0-9]{2}-[0-9]{2}-[0-9]{4}")) {
    			msg = "START_DATE code should be in the form DD-MM-YYYY"; 		
    		} else {
    			msg = "START_DATE code should have a valid date";
    		}
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ImportMarketFaultMsg(msg, fault);
    	}
		
		//checking endDate
		if (parameters.getEndDate().matches(dayRegex + "-" + monthRegex + "-" + yearRegex)) {
			//processing for URL parameters
			String[] date = parameters.getEndDate().split("-");
			e = date[0];
			//adjusting the month since January is 00
			d = Integer.toString(Integer.parseInt(date[1]) - 1);
			f = date[2];
    	} else {
    		String msg;
    		String code= "ERR_END_DATE";
    		
    		if (!parameters.getEndDate().matches("[0-9]{2}-[0-9]{2}-[0-9]{4}")) {
    			msg = "END_DATE code should be in the form DD-MM-YYYY"; 		
    		} else {
    			msg = "END_DATE code should have a valid date";
    		}
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ImportMarketFaultMsg(msg, fault);
    	}
		
		//checking dataSourceURL
		if (!parameters.getDataSource().matches("https*://*")) {
    		String msg = "DATA_SOURCE code should be a http:// URL";
    		String code= "ERR_DATA_SOURCE";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ImportMarketFaultMsg(msg, fault);
		} else {
			//processing for URL parameters
			//TODO: how are we supposed to realistically do this if they weren't always using the same url for yahoo?
		}
		
		
        //TODO: fix this up since it should only have eventSetId and not all of this junk
		StringBuilder sbf = new StringBuilder();
        sbf.append("Security Code: ").append(parameters.sec).append("\r\n");
        sbf.append("Start date: ").append(parameters.startDate).append("\r\n");
        sbf.append("End date: ").append(parameters.endDate).append("\r\n");
        sbf.append("Data source: ").append(parameters.dataSource).append("\r\n");
        
		ImportMarketDataResponse res = factory.createImportMarketDataResponse();
		res.returnData = sbf.toString();
		
		//mapping the parameters in SOAP request to URL parameters
		//TODO: is it fine that we specify the url based on yahoo's tables at
		// http://real-chart.finance.yahoo.com/table.csv?s=BHP.AX&a=00&b=01&c=2010&d=11&e=31&f=2010&g=d&ignore=.csv ?
		URL url;
		try {
			url = new URL("http://real-chart.finance.yahoo.com/table.csv?s=" + s 
						+ ".AX&a=" + a + 
						"&b=" + b + 
						"&c=" + c + 
						"&d=" + d + 
						"&e=" + e + 
						"&f=" + f + 
						"&g=d&ignore=.csv");
			
			//read content of returned page into file
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			//TODO: eventSetID needs to be generated, which I haven't done yet
			eventSetID = "test";
			String fileName = "/savedData/" + eventSetID + ".csv";
			File file = new File(fileName);
			
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter writer = new BufferedWriter(fw);
			
			String input;
			while ((input = in.readLine()) != null) {
				writer.write(input);
			}
			
			// TODO: Import Market Data operation, importMarketData() points 2-5, eventSetID
			in.close();
			writer.close();
			
		} catch (MalformedURLException e1) {
			// Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// Auto-generated catch block
			e1.printStackTrace();
		}
		
		return res;
	}
	
	
	@Override
	public DownloadFileResponse downloadFile(DownloadFileRequest parameters)
			throws DownloadFileFaultMsg {
		
		//check for errors in input here and return faultmsg as appropriate
		if (!parameters.getEventSetID().equals("abc-abc-111")) {
    		//assume that we only know about an eventSetId abc-abc-111
    		String msg = "Unknonw eventSetId was given";
    		String code = "ERR_EVENT";
 
       		//TODO: SOAP Fault handling should come here ...
    		//TODO: create a ServiceFaultType 'fault' from ObjectFactory
    		ServiceFaultType fault = factory.createServiceFaultType();
    		//TODO: prepare 'fault' object: set errorcode and errortext
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		//TODO: throw new DownloadFaultMsg(msg,fault)
    		throw new DownloadFileFaultMsg(msg, fault);
    	}
		
		DownloadFileResponse res = factory.createDownloadFileResponse();
		res.returnData = "EventSet Id: " + parameters.eventSetID;
		return res;
	}


}
