package au.edu.unsw.soacourse.marketservice;

import javax.jws.WebParam;

@WebService(endpointInterface="au.edu.unsw.soacourse.marketservice.TopDownSimpleService")
public class TopDownSimpleServiceImpl implements TopDownSimpleService {

	ObjectFactory factory = new ObjectFactory();

	@Override
	public ImportMarketDataResponse importMarketData(
			ImportMarketDataRequest parameters) throws ImportMarketFaultMsg {

		//check for errors in input here and return faultmsg as appropriate
		
		if (parameters.getSec().length() != 3) {
    		//assume that we only want a SEC value of length 3
    		String msg = "SEC code should be exactly 3 characters long";
    		String code= "ERR_SEC";
    		
    		//TODO: SOAP Fault handling should come here ...
    		//TODO: create a ServiceFaultType 'fault' from ObjectFactory
    		ServiceFaultType fault = factory.createServiceFaultType();
    		//TODO: prepare 'fault' object: set errorcode and errortext
    		fault.setErrcode(code);
    		fault.setErrtest(msg);
    		//TODO: throw new importMarketFaultMsg(msg,fault)
    		throw new importMarketFaultMsg(msg, fault);
    		
    	}
		
		
		StringBuilder sbf = new StringBuilder();
        sbf.append("Security Code: ").append(req.sec).append("\r\n");
        sbf.append("Start date: ").append(req.startDate).append("\r\n");
        sbf.append("End date: ").append(req.endDate).append("\r\n");
        sbf.append("Data source: ").append(req.dataSource).append("\r\n");
        
		ImportMarketDataResponse res = factory.createImportMarketDataResponse();
		res.setReturn(sbf.toString());
	}
	
	
	@Override
	public DownloadFileResponse downloadFile(DownloadFileRequest parameters)
			throws DownloadFileFaultMsg {
		
		//check for errors in input here and return faultmsg as appropriate
		if (!parameters.getEventSetID().equals("abc-abc-111")) {
    		//assume that we only know about an eventSetId abc-abc-111
    		String msg = "Unknonw eventSetId was given";
    		String code = "ERR_EVENT";
 
       		//TODO: SOAP Fault handling should come here ...
    		//TODO: create a ServiceFaultType 'fault' from ObjectFactory
    		ServiceFaultType fault = factory.createServiceFaultType();
    		//TODO: prepare 'fault' object: set errorcode and errortext
    		fault.setErrcode(code);
    		fault.setErrtest(msg);
    		//TODO: throw new DownloadFaultMsg(msg,fault)
    		throw new DownloadFaultMsg(msg, fault);
    	}
		
		DownloadFileResponse res = factory.createDownloadFileResponse();
		res.setReturn("EventSet Id: " + req.eventSetID);
		return res;
	}


}
